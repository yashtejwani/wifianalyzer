package com.example.wifianalyzer;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


public class MainActivity extends AppCompatActivity {

    String wifis[];

    Context context;
    WifiManager wifiManager;
    WifiInfo info;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        info = wifiManager.getConnectionInfo();


        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)){
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }else{
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }

        }
        statusCheck();
        showWifiDetails();
        }


    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(this.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults){
        switch (requestCode){
            case 1: {
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    if (ContextCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED){
                        Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }
    public void showWifiDetails() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifiManager.getConnectionInfo();

        TextView connected_ssid = (TextView) findViewById(R.id.connected_ssid);
        TextView net_id =(TextView) findViewById(R.id.net_id);
        TextView router_ip = (TextView) findViewById(R.id.router_ip);
        TextView ip_address = (TextView) findViewById(R.id.ip_address);
        TextView freq = (TextView) findViewById(R.id.freq);
      //  TextView tx_link = (TextView) findViewById(R.id.tx_link);
        TextView rx_link = (TextView) findViewById(R.id.rx_link);
        TextView rrsi = (TextView) findViewById(R.id.rssi);
        Button scan = (Button) findViewById(R.id.scan);
        TextView speedtest = (TextView) findViewById(R.id.speedtest);

        connected_ssid.setText(info.getSSID());
        net_id.setText("Network id: "+info.getNetworkId());
        ip_address.setText("IP Address: " + Formatter.formatIpAddress(info.getIpAddress()));
        router_ip.setText("Router IP: " + Formatter.formatIpAddress(wifiManager.getDhcpInfo().gateway));
        freq.setText("Frequency: " + info.getFrequency()+"MHz");
        ImageView levelImage2 = (ImageView) findViewById(R.id.levelImage2);
        rrsi.setText(info.getRssi() + "dbm");
        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AccessPointListDetail.class);
                startActivity(intent);
                finish();

            }
        });

        speedtest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 SpeedTestTask stt = new SpeedTestTask();
                       stt.execute();
            }

        });


        int signum = wifiManager.calculateSignalLevel(info.getRssi(), 5);
        switch (signum) {
            case 0:
                levelImage2.setImageResource(R.drawable.ic_signal_wifi_0_bar);
                break;
            case 1:
                levelImage2.setImageResource(R.drawable.ic_signal_wifi_1_bar);
                break;
            case 2:
                levelImage2.setImageResource(R.drawable.ic_signal_wifi_2_bar);
                break;
            case 3:
                levelImage2.setImageResource(R.drawable.ic_signal_wifi_3_bar);
                break;
            case 4:
                levelImage2.setImageResource(R.drawable.ic_signal_wifi_4_bar);
                break;

        }

     /*   ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isConnected = cm.getActiveNetworkInfo().isConnected();

        if(isConnected) {
           NetworkCapabilities nc = cm.getNetworkCapabilities(cm.getActiveNetwork());
           int txspeed = nc.getLinkDownstreamBandwidthKbps()/1024;
           int rxspeed = nc.getLinkUpstreamBandwidthKbps()/1024;

           tx_link.setText(String.valueOf(txspeed));
           rx_link.setText(String.valueOf(rxspeed));
        }
*/


    }
@Override
public void onResume() {
    super.onResume();
    showWifiDetails();
   // setView();
  //  getWifiDetails();
}
}
