package com.example.wifianalyzer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.example.wifianalyzer.R.color.success;

public class AccessPointAdapter extends ArrayAdapter<ScanResult> {
    private Context mContext;
    private int mResource;
    private List<ScanResult> wifidata;
    WifiManager wifiManager;




    public AccessPointAdapter(Context context, int resouce, List<ScanResult> wifilist) {
        super(context, resouce, wifilist);
        mContext = context;
        mResource = resouce;
        wifidata = (List<ScanResult>) (wifilist);
        wifiManager = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

    }

    @SuppressLint("ResourceAsColor")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        WifiInfo info = wifiManager.getConnectionInfo();

        Iterator<ScanResult> itr = wifidata.listIterator();
        System.out.println("Inside Adapter class");

        ScanResult accessDetails = wifidata.get(position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView ssid = (TextView) convertView.findViewById(R.id.ssid);
        TextView level = (TextView) convertView.findViewById(R.id.level);
        TextView channel = (TextView) convertView.findViewById(R.id.channel);
        TextView pFreq = (TextView) convertView.findViewById(R.id.primaryFrequency);
        TextView capab = (TextView) convertView.findViewById(R.id.capabilities);
        TextView vendor = (TextView) convertView.findViewById(R.id.vendor);
        while (itr.hasNext()) {
            System.out.println("Inside Adapter");
            System.out.println(itr.next());
            //netssid = itr.next();
        }

        //System.out.println("Level"+ signum);
        ssid.setText(accessDetails.SSID + " " + accessDetails.BSSID);
        level.setText(accessDetails.level + "dBm");
        channel.setText(String.valueOf(accessDetails.channelWidth));
        pFreq.setText(accessDetails.frequency + "MHz");
        capab.setText(accessDetails.capabilities);
        vendor.setText(accessDetails.venueName);
        setLevelImage(accessDetails, convertView);

        if(accessDetails.SSID.equals(info.getSSID())) {
            System.out.println("checking wifi same");
            ssid.setBackgroundColor(success);
        }

        return convertView;
    }


    public void setLevelImage(ScanResult accessDetails, View view) {
        WifiManager manager = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        ImageView levelImage = (ImageView) view.findViewById(R.id.levelImage);

        int signum = manager.calculateSignalLevel(accessDetails.level, 5);
        switch (signum) {
            case 0:
                levelImage.setImageResource(R.drawable.ic_signal_wifi_0_bar);
                break;
            case 1:
                levelImage.setImageResource(R.drawable.ic_signal_wifi_1_bar);
                break;
            case 2:
                levelImage.setImageResource(R.drawable.ic_signal_wifi_2_bar);
                break;
            case 3:
                levelImage.setImageResource(R.drawable.ic_signal_wifi_3_bar);
                break;
            case 4:
                levelImage.setImageResource(R.drawable.ic_signal_wifi_4_bar);
                break;

        }

    }
}