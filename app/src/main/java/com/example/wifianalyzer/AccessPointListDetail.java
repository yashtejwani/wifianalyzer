package com.example.wifianalyzer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.text.format.Formatter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

import java.util.List;

import static android.text.format.Formatter.formatIpAddress;

public class AccessPointListDetail extends AppCompatActivity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.access_point_list);
        getWifiDetails();
    }

    public void getWifiDetails() {
        // statusCheck();
        String[] wifis;

        ListView list = findViewById(R.id.list);
         WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled()) {
            Toast.makeText(this.getApplicationContext(), "Turning WiFi ON...", Toast.LENGTH_LONG).show();
            wifiManager.setWifiEnabled(true);
        }
        wifiManager.startScan();
        List<ScanResult> wifilist = (List<ScanResult>) wifiManager.getScanResults();
        // ArrayList<String> deviceList = new ArrayList<>();

        wifis = new String[wifilist.size()];
     /*   for(int i = 0; i < wifilist.size(); i++){
            wifis[i] = ((wifilist.get(i)).toString());
        }
        String filtered[] = new String[wifilist.size()];
        int counter = 0;
        for (String eachWifi : wifis) {
            String[] temp = eachWifi.split(",");
            filtered[counter] = temp[0] +temp[2] +temp[3];//0->SSID, 2->Key Management 3-> Strength
            counter++;
        } */
        //Iterator<ScanResult> itr = wifilist.listIterator();
        //while(itr.hasNext()) {
        //   System.out.println(itr.next());
        //}
        for(String ele: wifis) {
            System.out.println(ele);
        }
        //for (ScanResult scanResult : wifilist) {
        //sb.append("\n").append(scanResult.SSID).append(" - ").append(scanResult.capabilities);
        //    deviceList.add(scanResult.SSID + "---" + scanResult.capabilities);
        // }
        // Toast.makeText(context, sb, Toast.LENGTH_SHORT).show();
        //   ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, wifis);
        AccessPointAdapter adapter = new AccessPointAdapter(this,R.layout.access_point_details,wifilist);
        list.setAdapter(adapter);

    }


}
